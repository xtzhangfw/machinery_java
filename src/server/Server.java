package server;

import backend.AsyncResult;
import backend.Backend;
import backend.ChainAsyncResult;
import backend.ChordAsyncResult;
import broker.Broker;
import channel.Channel;
import config.Config;
import task.*;
import worker.Worker;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;

public class Server {
    public Config config;
    public HashMap<String, Task> registeredTasks;
    public Broker broker;
    public Backend backend;
    public static Logger logger = Logger.getLogger(Server.class.getName());

    public Server(Config cnf) throws  Exception{
        try {
            broker = new Broker(cnf);
        }catch (Exception e){
            throw new Exception("Can't create broker.\n");
        }
        try {
            backend = new Backend(cnf);
        }catch (Exception e){
            throw new Exception("Can't create backend.\n");
        }

        config = cnf;
        registeredTasks = new HashMap<String, Task>();
    }

    public String[] getRegisteredTaskNames(){
        String[] taskNames = new String[registeredTasks.size()];
        int i=0;
        for(Map.Entry<String,Task> entry: registeredTasks.entrySet()){
            taskNames[i] = entry.getKey();
            i++;
        }
        return taskNames;
    }

    public void registerTask(String name, Task taskObj){
        registeredTasks.put(name, taskObj);
        broker.setRegisteredTaskName(getRegisteredTaskNames());
    }

    public boolean isTaskRegistered(String name) {
        return registeredTasks.containsKey(name);
    }
    public Task getRegisteredTask(String name){
        if(registeredTasks.containsKey(name)){
            return registeredTasks.get(name);
        }
        return null;
    }

    public AsyncResult sendTask(Signature signature) throws Exception{
        if(backend==null){
            throw new Exception("Backend is null.\n");
        }
        if(signature.uuid.equals("")) {
            signature.uuid = UUID.randomUUID().toString();
        }
        try {
            backend.setStatePending(signature);
        }catch (Exception e){
            throw new Exception("Set pending state error.\n");
        }

        try{
            broker.publish(signature);
        }catch (Exception e){
            throw new Exception("Public message error.\n" + e.getMessage());
        }

        return new AsyncResult(signature, backend);
    }

    public ChainAsyncResult sendChain(Chain chain) throws Exception{
        try{
            sendTask(chain.tasks[0]);
            return new ChainAsyncResult(chain.tasks, backend);

        }catch (Exception e){
            return null;
        }
    }

    public AsyncResult[] sendGroup(Group group){
        if(backend == null) { return null; }
        AsyncResult[] asyncResults = new AsyncResult[group.tasks.length];
        try {
            backend.initGroup(group.groupUUID, group.getUUIDs());
        }catch (Exception e){
            return null;
        }

        Channel<Integer> wg = new Channel<Integer>(group.tasks.length);
        Channel<Exception> errorChan = new Channel<Exception>(1);

        for(int i=0; i<group.tasks.length; i++){
            wg.push(1);
        }

        for(int i=0; i<group.tasks.length; i++){
            final int index = i;
            final Signature signature = group.tasks[i];
            new Thread(){
                @Override
                public void run() {
                    try {
                        backend.setStatePending(group.tasks[index]);
                        broker.publish(signature);
                        asyncResults[index] = new AsyncResult(signature, backend);
                    }catch (Exception e){
                        errorChan.push(new Exception("setState or publish msg error"), false);
                    }
                    finally {
                        wg.pop();
                    }
                }
            }.start();
        }

        Channel<Integer> done = new Channel<Integer>(1);
        new Thread(){
            @Override
            public void run() {
                wg.waitUntilEmpty();
                done.push(1);
            }
        }.start();

        while(true){
            if(errorChan.pop(false)!=null){
                return asyncResults;
            }
            if(done.pop(false)!=null){
                return asyncResults;
            }
            try{ sleep(1); }catch (Exception e){}
        }
    }

    public ChordAsyncResult sendChord(Chord chord){
        AsyncResult[] groupAsyncResults = sendGroup(chord.group);
        if(groupAsyncResults.length<=0) return null;
        for(int i=0; i<groupAsyncResults.length; i++){
            if(groupAsyncResults[i] == null) return null;
        }
        return new ChordAsyncResult(chord.group.tasks, chord.callback, backend);
    }


    public Worker newWorker(String consumerTag, int concurrency){
        return new Worker(this, consumerTag, concurrency);
    }





}
