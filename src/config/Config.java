package config;

import com.google.gson.Gson;
import java.io.*;

public class Config {
    public String brokerHost;
    public int brokerPort;
    public int brokerDB;
    public String brokerPassword;

    public String backendHost;
    public int backendPort;
    public int backendDB;
    public String backendPassword;

    public String defaultQueue;
    public int maxWorkerInstance;
    public int resultExpireIn;

    public Config(){}

    public Config(String cfgFile) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(cfgFile)));
        String jsonstr="";
        String line;
        while((line = br.readLine())!=null){
            jsonstr += line;
        }
        Config cnf_tmp = initFromJson(jsonstr);
        this.brokerHost = cnf_tmp.brokerHost;
        this.brokerPort = cnf_tmp.brokerPort;
        this.brokerDB = cnf_tmp.brokerDB;
        this.brokerPassword = cnf_tmp.brokerPassword;

        this.backendHost = cnf_tmp.backendHost;
        this.backendPort = cnf_tmp.backendPort;
        this.backendDB = cnf_tmp.backendDB;
        this.backendPassword = cnf_tmp.backendPassword;

        this.defaultQueue = cnf_tmp.defaultQueue;
        this.maxWorkerInstance = cnf_tmp.maxWorkerInstance;
        this.resultExpireIn = cnf_tmp.resultExpireIn;

    }

    public static Config initFromJson(String s){
        Gson gson = new Gson();
        return gson.fromJson(s, Config.class);
    }

    static public void main(String[] args) throws Exception{
        Config cfg = new Config("config.json");
        Gson gson = new Gson();
        String str = gson.toJson(cfg);
        System.out.println(str);
    }

}
