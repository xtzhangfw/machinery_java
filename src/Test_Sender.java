import backend.AsyncResult;
import backend.ChordAsyncResult;
import config.Config;
import server.Server;
import task.*;
import worker.Worker;



public class Test_Sender {
    public static void main(String[] cmdargs) throws Exception{
        Add add = new Add();
        Multiply multiply = new Multiply();

        Config cnf = new Config("config.json");
        Server server = new Server(cnf);
        server.registerTask("add", add);
        server.registerTask("multiply", multiply);

        Arg[] args1 = new Arg[2];
        args1[0] = new Arg(); args1[1]=new Arg();
        args1[0].type = "Integer"; args1[1].type = "Integer";
        args1[0].value = "1"; args1[1].value = "0";

        Arg[] args2 = new Arg[2];
        args2[0] = new Arg(); args2[1]=new Arg();
        args2[0].type = "Integer"; args2[1].type = "Integer";
        args2[0].value = "1"; args2[1].value = "0";


        Signature s1 = new Signature("add", args1);
        Signature s2 = new Signature("add", args2);
        Signature s3 = new Signature("add", null);

        Signature[] slist = new Signature[20];
        for(int i=0; i<slist.length; i++){
            if(i%2==0){
                slist[i]=new Signature("add", args1);
            }
            else{
                slist[i]=new Signature("add", args2);
            }
        }

        Group group = new Group(slist);
        Chord chord = new Chord(group,s3);

        ChordAsyncResult chordRes = server.sendChord(chord);
        System.out.println(chordRes.get(1000)[0].value);
    }

}
