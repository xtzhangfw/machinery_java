package backend;

import channel.Channel;
import task.Arg;
import task.Signature;
import task.TaskState;

public class AsyncResult {
    public Signature signature;
    public TaskState taskState;
    public Backend backend;

    public AsyncResult(Signature signature, Backend backend){
        this.signature = signature;
        this.taskState = new TaskState(signature, TaskState.StatePending);
        this.backend = backend;
    }

    public Arg[] get(int sleepDuration){
        Channel<Integer> timeChan = new Channel<Integer>(1);
        new Thread(){
            @Override
            public void run(){
                try {
                    sleep(sleepDuration);
                }catch (Exception e){

                }finally {
                    timeChan.push(1);
                }
            }
        }.start();

        while(true){
            if(timeChan.pop(false)!=null){break;}
            getState();
            if(taskState!=null && taskState.isSuccess()){
                Arg[] resultValues = new Arg[taskState.taskResult.length];
                for(int i=0; i<taskState.taskResult.length; i++){
                    resultValues[i] = new Arg();
                    resultValues[i].value = taskState.taskResult[i].value;
                    resultValues[i].type = taskState.taskResult[i].type;
                }
                return resultValues;
            }
        }
        return null;
    }

    public TaskState getState(){
        if(taskState!=null && taskState.isCompleted()){
            return taskState;
        }

        taskState = backend.getState(signature.uuid);
        return taskState;
    }
}
