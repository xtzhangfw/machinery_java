package backend;

import com.google.gson.Gson;
import common.RedisConnector;
import config.Config;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;
import task.*;

public class Backend {
    public Config cnf;
    public String host;
    public int port;
    public String password;
    public int db;
    public RedisConnector pool;

    public Backend(Config config){
        cnf = config;
        host = cnf.backendHost;
        port = cnf.backendPort;
        password = cnf.backendPassword;
        db = cnf.backendDB;
    }

    public void initGroup(String groupUUID, String[] taskUUIDs) throws Exception{
        GroupMeta groupMeta = new GroupMeta(groupUUID, taskUUIDs);
        Gson gson = new Gson();
        String encoded = gson.toJson(groupMeta);

        Jedis conn = openConn();
        conn.set(groupUUID, encoded);
        setExpirationTime(groupUUID);
        conn.close();
    }

    public GroupMeta getGroupMeta(String groupUUID) throws Exception{
        Jedis conn = openConn();
        String item = conn.get(groupUUID);
        Gson gson = new Gson();
        GroupMeta groupMeta =  gson.fromJson(item, GroupMeta.class);
        conn.close();
        return groupMeta;
    }

    public boolean groupCompleted(String groupUUID, int groupTaskCount){
        try {
            GroupMeta groupMeta = getGroupMeta(groupUUID);
            int ln = groupMeta.taskUUIDs.length;
            int cnt = 0;
            for (int i = 0; i < ln; i++) {
                TaskState taskState = getState(groupMeta.taskUUIDs[i]);
                if(taskState!=null && taskState.isCompleted()){
                    cnt++;
                }
            }

            return cnt == groupTaskCount;
        }catch (Exception e){
            return false;
        }
    }

    public TaskState getState(String taskUUID){
        try{
            Jedis conn = openConn();
            String reply = conn.get(taskUUID);
            Gson gson = new Gson();
            TaskState taskState = gson.fromJson(reply, TaskState.class);
            conn.close();
            return taskState;
        }catch (Exception e){
            return null;
        }
    }

    public TaskState[] groupTaskStates(String groupUUID, int groupTaskCount){
        try {
            GroupMeta groupMeta = getGroupMeta(groupUUID);
            TaskState[] taskStates = new TaskState[groupMeta.taskUUIDs.length];
            for(int i=0; i<groupMeta.taskUUIDs.length; i++){
                taskStates[i] = getState(groupMeta.taskUUIDs[i]);
            }
            return taskStates;
        }catch (Exception e){
            return null;
        }
    }


    public void updateState(TaskState taskState) throws Exception{
        Jedis conn = openConn();
        Gson gson = new Gson();
        String encoded = gson.toJson(taskState);
        conn.set(taskState.taskUUID, encoded);
        setExpirationTime(taskState.taskUUID);
    }

    public boolean triggerChord(String groupUUID){
        try {
            Jedis conn = openConn();
            String lockName = "TriggerChordMutex";
            pool.lock(lockName, 10000);
            GroupMeta groupMeta = getGroupMeta(groupUUID);
            if(groupMeta.chordTriggered){
                pool.unlock(lockName);
                return false;
            }
            groupMeta.chordTriggered = true;
            Gson gson = new Gson();
            String encoded = gson.toJson(groupMeta, GroupMeta.class);
            conn.set(groupUUID, encoded);
            pool.unlock(lockName);
            conn.close();
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public void setExpirationTime(String key) throws Exception{
        int expiresIn = cnf.resultExpireIn;
        if(expiresIn==0) {
            expiresIn = 3600;
        }

        long expirationTimestamp = System.currentTimeMillis()/1000 + expiresIn;
        Jedis jedis = openConn();
        openConn().expireAt(key, expirationTimestamp);
        jedis.close();
    }

    public void setStatePending(Signature signature) throws Exception{
        TaskState taskState = TaskState.newPendingTaskState(signature);
        updateState(taskState);
    }

    public void setStateReceived(Signature signature) throws Exception{
        TaskState taskState = TaskState.newReceivedTaskState(signature);
        updateState(taskState);
    }

    public void setStateStarted(Signature signature) throws Exception{
        TaskState taskState = TaskState.newStartedTaskState(signature);
        updateState(taskState);
    }

    public void setStateRetry(Signature signature) throws Exception{
        TaskState taskState = TaskState.newRetryTaskState(signature);
        updateState(taskState);
    }

    public void setStateSuccess(Signature signature, Arg[] results) throws Exception{
        TaskState taskState = TaskState.newSuccessTaskState(signature, results);
        updateState(taskState);
    }

    public void setStateFailure(Signature signature) throws Exception{
        TaskState taskState = TaskState.newFailureTaskState(signature);
        updateState(taskState);
    }

    public void purgeState(String taskUUID) throws Exception{
        Jedis conn = openConn();
        conn.del(taskUUID);
        conn.close();
    }

    public void purgeGroupMeta(String groupUUID) throws Exception{
        Jedis conn = openConn();
        conn.del(groupUUID);
        conn.close();
    }


    public Jedis openConn() throws Exception{
        try {
            pool = new RedisConnector(host, port, password, db);
            return pool.getJedis();
        }catch(Exception e){
            throw new Exception("Connect to Redis Error");
        }
    }


    public static void main(String[] args){

    }




}
