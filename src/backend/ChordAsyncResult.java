package backend;

import task.Arg;
import task.Signature;

public class ChordAsyncResult {
    AsyncResult[] groupAsyncResults;
    AsyncResult chordAsyncResult;
    Backend backend;

    public ChordAsyncResult(Signature[] groupTasks, Signature chordCallback, Backend backend){
        this.groupAsyncResults = new AsyncResult[groupTasks.length];
        for(int i=0; i<groupTasks.length; i++){
            groupAsyncResults[i] = new AsyncResult(groupTasks[i], backend);
        }
        this.chordAsyncResult = new AsyncResult(chordCallback, backend);
        this.backend = backend;
    }

    public Arg[] get(int sleepDuration){
        if(backend==null){ return null; }
        for(int i=0; i<groupAsyncResults.length; i++){
            Arg[] result = groupAsyncResults[i].get(sleepDuration);
            if(result == null) { return null; }
        }
        return chordAsyncResult.get(sleepDuration);
    }
}
