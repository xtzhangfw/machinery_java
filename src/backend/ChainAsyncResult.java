package backend;

import channel.Channel;
import task.Arg;
import task.Signature;

public class ChainAsyncResult {
    AsyncResult[] asyncResults;
    Backend backend;

    public ChainAsyncResult(Signature[] tasks, Backend backend){
        this.asyncResults = new AsyncResult[tasks.length];
        for(int i=0; i<tasks.length; i++){
            this.asyncResults[i] = new AsyncResult(tasks[i], backend);
        }
        this.backend = backend;
    }

    public Arg[] get(int sleepDuration){
        if(backend==null){ return null; }
        Channel<Integer> timeChan = new Channel<Integer>(1);
        new Thread(){
            @Override
            public void run(){
                try {
                    sleep(sleepDuration);
                }catch (Exception e){

                }finally {
                    timeChan.push(1);
                }
            }
        }.start();

        while(true){
            if(timeChan.pop(false)==1){break;}
            Arg[] results = null;
            for(int i=0; i<asyncResults.length; i++){
                results = asyncResults[i].get(sleepDuration);
                if(results==null) return null;
            }
            return results;
        }
        return null;
    }
}
