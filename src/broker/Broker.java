package broker;

import channel.Channel;
import com.google.gson.Gson;
import common.RedisConnector;
import config.*;
import redis.clients.jedis.Jedis;
import task.Signature;
import worker.Worker;

import java.util.List;
import java.util.logging.Logger;

public class Broker {
    public Config cnf;
    public String[] registeredTaskNames;
    public String host;
    public int port;
    public String password;
    public int db;
    public RedisConnector pool;
    public Channel<Integer> stopReceivingChan;
    public Channel<Integer> stopChan;
    public Channel<Integer> receivingWG;

    boolean retry;

    public Channel<Integer> retryStopChan;

    public static Logger logger = Logger.getLogger(Broker.class.getName());

    public Broker(Config cnf) throws Exception{
        this.cnf = cnf;
        this.host = cnf.brokerHost;
        this.port = cnf.brokerPort;
        this.db = cnf.brokerDB;
        this.password = cnf.brokerPassword;
    }

    public void setRegisteredTaskName(String[] names){
        registeredTaskNames = names;
    }

    public boolean isTaskRegistered(String name) {
        for(int i=0; i<registeredTaskNames.length; i++){
            if(registeredTaskNames[i].equals(name)){
                return true;
            }
        }
        return false;
    }

    public void publish(Signature signature) throws Exception{
        Gson gson = new Gson();
        if(signature.routingKey==null || signature.routingKey.equals("")){
            signature.routingKey = cnf.defaultQueue;
        }
        String msg = gson.toJson(signature);
        try {
            Jedis conn = openConn();
            conn.rpush(cnf.defaultQueue, msg);
            conn.close();

        }catch (Exception e){
            throw new Exception("Broker.publish Error <- " + e.getMessage());
        }
    }

    public void stopConsuming(){
        retry=false;
        retryStopChan.push(1,false);

        stopReceivingChan.push(1);
        receivingWG.pop();

        stopChan.push(1);
    }

    public void startConsuming(int concurrency, Worker worker) throws Exception{
        stopChan = new Channel<Integer>(1);
        retryStopChan = new Channel<Integer>(1);
        stopReceivingChan = new Channel<Integer>(1);
        receivingWG = new Channel<Integer>(1);

        Channel<String> deliveries = new Channel<String>(1);

        new Thread(){
            @Override
            public void run() {
                while(true){
                    if(stopReceivingChan.pop(false)!=null) break;
                    try {
                        String task = nextTask(cnf.defaultQueue);
                        if(task!=null) {
                            logger.info(task);
                            deliveries.push(task);
                        }
                    }catch (Exception e){
                        continue;
                    }
                }
                receivingWG.push(1);
            }
        }.start();
        consume(deliveries, concurrency, worker);

    }

    public void consume(Channel<String> deliveries, int concurrency, Worker worker) throws Exception{
        Channel<Integer> poolChan = new Channel<Integer>(concurrency);
        new Thread(){
            @Override
            public void run() {
                for(int i=0; i<concurrency; i++){
                    poolChan.push(i);
                }
            }
        }.start();

        Channel<String> errorChan = new Channel<String>(1);
        Channel<Integer> wg = new Channel<Integer>(concurrency*2);

        while(true){
            if(stopChan.pop(false)!=null){
                wg.waitUntilEmpty();
                return;
            }

            String errorStr = errorChan.pop(false);
            if(errorStr!=null) {
                throw new Exception(errorStr);
            }

            String task = deliveries.pop(false);
            if(task!=null){
                if(concurrency>0){ poolChan.pop(); }
                wg.push(1);

                new Thread(){
                    @Override
                    public void run() {
                        try {
                            consumeOne(task, worker);
                        }catch (Exception e){
                            errorChan.push("ERROR: run task failed:" + task + "\n" + e.getMessage());
                        }
                        if(concurrency>0){ poolChan.push(1); }
                        wg.pop();
                    }
                }.start();
            }
        }

    }

    public void consumeOne(String delivery, Worker worker) throws Exception{

        Signature sig = null;
        try {
            Gson gson = new Gson();
            sig = gson.fromJson(delivery, Signature.class);
        }catch (Exception e){
            throw new Exception("Broker.consumeOne Error\n" + e.getMessage());
        }

        try {
            if(!isTaskRegistered(sig.name)) {
                Jedis jedis = openConn();
                jedis.rpush(cnf.defaultQueue, delivery);
                jedis.close();
            }
        }catch (Exception e){
                throw new Exception("Broker.consumeOne Error\n" + e.getMessage());
        }

        worker.Process(sig);
    }

    public String nextTask(String queue) throws Exception{
        List<String> res=null;
        Jedis conn = openConn();
        res = conn.blpop(1,queue);
        if(res.size()==2){
            return res.get(1);
        }else{
            return null;
        }
    }

    public Jedis openConn() throws Exception{
        try {
            pool = new RedisConnector(host, port, password, db);
            return pool.getJedis();
        }catch (Exception e){
            throw new Exception("Broker.openConn Error");
        }
    }

    public static void main(String[] args){
    }

}
