package retry;

public class Retry {
    public static int fibonacciNext(int start){
        int a=0, b=1;
        while(a<=start){
            int c = a + b;
            a = b; b = c;
        }
        return a;
    }
}
