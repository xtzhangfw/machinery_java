package common;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import static java.lang.Thread.sleep;


public class RedisConnector {
    public JedisPool pool;
    public String password;
    public String host;
    public int db;
    public int port;
    public int timeout;

    public RedisConnector(String host, int port, String password, int db) throws Exception{
        this.host = host;
        this.port = port;
        this.password = password;
        this.db = db;
        this.timeout = 1000;

        if(password.length()<=0){
            this.pool = new JedisPool(new JedisPoolConfig(), host, port);
        }else {
            this.pool = new JedisPool(new JedisPoolConfig(), host, port, timeout, password);
        }
    }

    public Jedis getJedis() throws Exception{
        Jedis jedis = pool.getResource();
        jedis.select(db);
        return jedis;
    }

    public String getInnerLockName(String lockName){
        return "JAVAREDISLOCK_" + lockName;
    }

    public void lock(String lockName, int timeout) throws Exception{
        String innerLockName = getInnerLockName(lockName);
        Jedis jedis = getJedis();

        while(true){
            long currentStamp = System.currentTimeMillis();
            long timeStamp = System.currentTimeMillis() + timeout;
            if(jedis.setnx(innerLockName,String.valueOf(timeStamp))==1){
                break;
            }else{
                currentStamp = System.currentTimeMillis();
                if(Long.valueOf(jedis.get(innerLockName)) <= currentStamp){
                    timeStamp = System.currentTimeMillis() + timeout;
                    Long res = Long.valueOf(jedis.getSet(innerLockName, String.valueOf(timeStamp)));
                    if(res < timeStamp){ break; }
                }
            }
            sleep(1);
        }

        new Thread(){
            @Override
            public void run(){
                while(true) {
                    try {
                        sleep(1);
                        Jedis jedis = getJedis();
                        if(Long.valueOf(jedis.get(innerLockName)) >= System.currentTimeMillis()){
                            jedis.del(innerLockName);
                        }
                        jedis.close();
                    } catch (Exception e) {}

                }
            }
        }.start();

        jedis.close();
    }

    public void unlock(String lockName) throws Exception{
        String innerLockName = getInnerLockName(lockName);
        Jedis jedis = getJedis();
        jedis.del(innerLockName);
        jedis.close();
    }

    public void closePool(){
        pool.close();
    }

    public static void main(String[] args) throws Exception{
        RedisConnector conn = new RedisConnector("localhost", 6379, "", 0);
        Jedis jedis = conn.getJedis();
        jedis.rpush("stu", "zxt", "wk", "xitong");
        System.out.println(jedis.blpop(1,"stu"));
        jedis.close();
    }
}
