import backend.AsyncResult;
import backend.ChordAsyncResult;
import config.Config;
import server.Server;
import task.*;
import worker.Worker;

import java.util.logging.Logger;


class Add extends Task{
    @Override
    public Object[] taskFunc(Object... args) {
        Object[] res = new Object[1];
        res[0] = Integer.valueOf(0);
        for(int i=0; i<args.length; i++) {
            res[0] = (Integer)res[0] + (Integer)args[i];
        }
        return res;
    }
}

class Multiply extends Task{
    @Override
    public Object[] taskFunc(Object... args) {
        Object[] res = new Object[1];
        for(int i=0; i<args.length; i++) {
            res[0] = (Integer)res[0] * (Integer)args[i];
        }
        return res;
    }
}


public class Test_Worker {
    public static void main(String[] cmdargs) throws Exception{
        Add add = new Add();
        Multiply multiply = new Multiply();

        Config cnf = new Config("config.json");
        Server server = new Server(cnf);
        server.registerTask("add", add);
        server.registerTask("multiply", multiply);

        Worker worker = server.newWorker("worker_name", 10);
        worker.Launch();
    }
}
