package task;

import java.util.UUID;

public class Group {
    public String groupUUID;
    public Signature[] tasks;

    public Group(Signature... signatures){
        groupUUID = UUID.randomUUID().toString();
        for(int i=0; i<signatures.length; i++){
            if(signatures[i].uuid.equals("")){
                signatures[i].uuid = UUID.randomUUID().toString();
            }
            signatures[i].groupUUID = groupUUID;
            signatures[i].groupTaskCount = signatures.length;
        }
        tasks = signatures;
    }

    public String[] getUUIDs(){
        String[] taskUUIDs = new String[tasks.length];
        for(int i=0; i<tasks.length; i++){
            taskUUIDs[i] = tasks[i].uuid;
        }
        return taskUUIDs;
    }
}
