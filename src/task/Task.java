package task;

import java.util.HashMap;

public abstract class Task {


    public abstract Object[] taskFunc(Object... args);

    public Arg[] call(Arg[] args) throws Exception{
        Object[] taskArgs = new Object[args.length];

        for(int i=0; i<args.length; i++){
            if(args[i].type.equals("Boolean")){
                taskArgs[i] =  Boolean.valueOf(args[i].value);
            }
            else if(args[i].type.equals("Integer")){
                taskArgs[i] = Integer.valueOf(args[i].value);
            }
            else if(args[i].type.equals("Float")){
                taskArgs[i] = Float.valueOf(args[i].value);
            }
            else if(args[i].type.equals("Double")){
                taskArgs[i] = Double.valueOf(args[i].value);
            }
            else if(args[i].type.equals("Long")){
                taskArgs[i] = Long.valueOf(args[i].value);
            }
            else if(args[i].type.equals("String")){
                taskArgs[i] = args[i].value;
            }else{
                throw new Exception("ERROR: args type:" + args[i].type + "\n");
            }
        }

        Object[] taskResults = taskFunc(taskArgs);
        Arg[] results = new Arg[taskResults.length];

        for(int i=0; i<results.length; i++){
            results[i] = new Arg();
            results[i].type = taskResults[i].getClass().getSimpleName();
            results[i].value = taskResults[i].toString();
        }
        return results;
    }
}
