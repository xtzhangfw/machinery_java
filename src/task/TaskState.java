package task;

import com.sun.net.httpserver.Authenticator;

public class TaskState {

    public static final String StatePending = "PENDING";
    public static final String StateReceived = "RECEIVED";
    public static final String StateStarted = "STARTED";
    public static final String StateRetry = "RETRY";
    public static final String StateSuccess = "SUCCESS";
    public static final String StateFailure = "FAILURE";

    public String taskUUID;
    public String state;
    public Arg[] taskResult;
    public String error;

    public TaskState(Signature signature, String state){
        this.taskUUID = signature.uuid;
        this.state = state;
    }

    public boolean isSuccess(){
        return this.state.equals(StateSuccess);
    }

    public boolean isFailure(){
        return this.state.equals(StateFailure);
    }

    public boolean isCompleted(){
        return this.isFailure() || this.isSuccess();
    }

    public static TaskState newPendingTaskState(Signature signature){
        return new TaskState(signature, StatePending);
    }

    public static TaskState newReceivedTaskState(Signature signature){
        return new TaskState(signature, StateReceived);
    }

    public static TaskState newStartedTaskState(Signature signature){
        return new TaskState(signature, StateStarted);
    }

    public static TaskState newSuccessTaskState(Signature signature, Arg[] results){
        TaskState taskState = new TaskState(signature, StateSuccess);
        taskState.taskResult = results;
        return taskState;
    }

    public static TaskState newFailureTaskState(Signature signature){
        return new TaskState(signature, StateFailure);
    }

    public static TaskState newRetryTaskState(Signature signature){
        return new TaskState(signature, StateRetry);
    }
}


