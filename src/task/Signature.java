package task;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Signature {
    public String uuid;
    public String name;
    public String routingKey;
    public String groupUUID;
    public int groupTaskCount;
    public Arg[] args;
    public boolean immutable;
    public int retryCount;
    public int retryTimeout;
    public Signature[] onSuccess;
    public Signature[] onError;
    public Signature chordCallback;


    public Signature(String name, Arg[] args){
        this.uuid = UUID.randomUUID().toString();
        this.name = name;
        this.args = args;
        this.groupUUID = "";
        this.routingKey = "";
    }


    public static void main(String[] args) throws  Exception{
        Signature a = new Signature("test", null);
        Signature b = new Signature("testb", null);
        a.chordCallback = b;
        Gson gson = new Gson();
        System.out.println(gson.toJson(a));
        throw new Exception("hello,world");

    }
}
