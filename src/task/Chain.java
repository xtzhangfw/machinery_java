package task;

import java.util.UUID;

public class Chain {
    public Signature[] tasks;

    public Chain(Signature[] signatures){
        for(int i=0; i<tasks.length; i++){
            if(signatures[i].uuid.equals("")){
                signatures[i].uuid = UUID.randomUUID().toString();
            }
        }

        for(int i=tasks.length-1; i>0; i--){
            if(i>0){
                signatures[i-1].onSuccess = new Signature[1];
                signatures[i-1].onSuccess[0] = signatures[i];
            }
        }
        tasks = signatures;
    }
}
