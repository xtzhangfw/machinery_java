package task;


public class GroupMeta {
    public String groupUUID;
    public String[] taskUUIDs;
    public boolean chordTriggered;
    public boolean lock;

    public GroupMeta(String groupUUID, String[] taskUUIDs){
        this.groupUUID = groupUUID;
        this.taskUUIDs = taskUUIDs;
    }
}
