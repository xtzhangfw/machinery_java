package task;

import com.google.gson.Gson;

public class Arg{
    public String type;
    public String value;

    public static void main(String[] args){
        Gson gson = new Gson();
        String s = "{\"type\":\"int\", \"value\":1}";
        Arg a = gson.fromJson(s,Arg.class);
        System.out.println(a.value);
    }

}