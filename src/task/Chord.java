package task;

import java.util.UUID;

public class Chord {
    public Group group;
    public Signature callback;

    public Chord(Group group, Signature callback){
        callback.uuid = UUID.randomUUID().toString();
        for(int i=0; i<group.tasks.length; i++){
            group.tasks[i].chordCallback = callback;
        }

        this.group = group;
        this.callback = callback;
    }

}
