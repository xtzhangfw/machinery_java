package channel;

import java.util.LinkedList;

import static java.lang.Thread.sleep;

public class Channel<T> {
    public LinkedList<T> chan;
    public int size;
    public Channel(int s){
        chan = new LinkedList<T>();
        size = s;
    }

    public void waitUntilEmpty(){
        boolean flag = false;
        while(!false){
            synchronized (this){
                if( chan.size() > 0 ){
                    try{this.wait();}catch (Exception e){}
                }else{
                    return;
                }
            }
        }
    }

    public void push(T obj){
        boolean flag = false;
        while(!flag) {
            synchronized (this) {
                if (chan.size() < size) {
                    chan.push(obj);
                    flag = true;
                    notifyAll();
                }
                else{
                    try{this.wait();}catch (Exception e){}
                }
            }
        }
    }


    public void push(T obj, boolean ifBlock){
        boolean flag = false;
        while(!flag) {
            synchronized (this) {
                if (chan.size() < size) {
                    chan.push(obj);
                    flag = true;
                    notifyAll();
                    return;
                }else{
                    if(ifBlock==false) return;
                    try{this.wait();}catch (Exception e){}
                }
            }
        }
    }
    public T pop(){
        boolean flag = false;
        T res = null;
        while(!flag) {
            synchronized (this) {
                if (chan.size() > 0) {
                    res = chan.pop();
                    flag = true;
                    notifyAll();
                }else{
                    try{this.wait();}catch (Exception e){}
                }
            }
        }
        return res;
    }

    public T pop(boolean ifBlock){
        boolean flag = false;
        T res = null;
        while(!flag) {
            synchronized (this) {
                if (chan.size() > 0) {
                    res = chan.pop();
                    flag = true;
                    notifyAll();
                    return res;
                }else{
                    if(ifBlock==false) return res;
                    try{this.wait();}catch (Exception e){}
                }
            }
        }
        return res;
    }

    public static void main(String[] args){
        Channel<Integer> chan = new Channel<Integer>(1);

        Thread t1 = new Thread(){
            @Override
            public void run() {
                for(int i=0; i<15; i++){
                    chan.push(i);
                    System.out.println("t1 push " + i);
                    try {
                        sleep(1);
                    }catch (Exception e){}
                }
            }
        };

        Thread t2 = new Thread(){
            @Override
            public void run() {
                for(int i=0; i<10; i++){
                    Integer ans = chan.pop();
                    System.out.println(ans);
                }
            }
        };

        t1.start();
        t2.start();
    }
}
