package worker;

import broker.Broker;
import channel.Channel;
import config.Config;
import retry.Retry;
import server.Server;
import task.Arg;
import task.Signature;
import task.Task;
import task.TaskState;

import java.util.ArrayList;
import java.util.logging.Logger;

public class Worker {
    public Server server;
    public String consumerTag;
    public int concurrency;
    public static Logger logger = Logger.getLogger(Worker.class.getName());

    public Worker(Server server, String consumerTag, int concurrency){
        this.server = server;
        this.consumerTag = consumerTag;
        this.concurrency = concurrency;
    }

    public void Quit(){
        server.broker.stopConsuming();
    }

    public void Launch(){
        Config cnf = server.config;
        Broker broker = server.broker;

        Channel<Exception> errorChan = new Channel<Exception>(1);
        Worker worker = this;

        new Thread(){
            @Override
            public void run() {
                while(true) {
                    try {
                        broker.startConsuming(concurrency, worker);
                    }catch (Exception e) {
                        logger.warning("Start consuming error.");
                    }
                }
            }
        }.start();
    }

    public void Process(Signature signature) throws Exception{
        if(!server.isTaskRegistered(signature.name)){
            return;
        }

        Task task = server.getRegisteredTask(signature.name);
        server.backend.setStateStarted(signature);

        Arg[] results = null;
        try {
            results = task.call(signature.args);
            taskSucceeded(signature, results);
        }catch (Exception e){
            if(signature.retryCount > 0){
                logger.warning("Task failed, retrying...");
                taskRetry(signature);
                throw new Exception("WARNING: task failed and retry " + signature.name + "\n" + e.getMessage());
            }
            else{
                logger.warning("Task failed, exiting...");
                taskFailed(signature);
                throw new Exception("ERROR: task failed and exit " + signature.name + "\n" + e.getMessage());
            }
        }
    }

    public void taskSucceeded(Signature signature, Arg[] results) throws Exception{
        try {
            server.backend.setStateSuccess(signature, results);
        }catch (Exception e){
            throw new Exception("ERROR: setStateSuccess failed\n");
        }

        for(int i=0; signature.onSuccess!=null && i<signature.onSuccess.length; i++){
            Signature successTask = signature.onSuccess[i];
            if(signature.immutable==false){
                successTask.args = results;
            }
            server.sendTask(successTask);
        }

        if(signature.groupUUID==null || signature.groupUUID.equals("")){return;}

        boolean groupCompleted=false;
        try{
            groupCompleted = server.backend.groupCompleted(signature.groupUUID, signature.groupTaskCount);
        }catch (Exception e){
            throw new Exception("Group Completed error");
        }

        if(!groupCompleted){ return; }

        if(signature.chordCallback==null){ return; }

        boolean shouldTrigger = false;
        try{
            shouldTrigger = server.backend.triggerChord(signature.groupUUID);
        }catch (Exception e){
            throw new Exception("Trigger chord error");
        }

        if(!shouldTrigger){ return; }
        TaskState[] taskStates = null;
        try {
            taskStates = server.backend.groupTaskStates(signature.groupUUID, signature.groupTaskCount);
        }catch (Exception e){
            throw new Exception("group taskstates error");
        }


        ArrayList<Arg> args = new ArrayList<Arg>();
        for(int i=0; i<taskStates.length; i++){
            if(!taskStates[i].isSuccess()) return;
            for(int j=0; j<taskStates[i].taskResult.length; j++){
                args.add(taskStates[i].taskResult[j]);
            }
        }

        signature.chordCallback.args = new Arg[args.size()];
        for(int i=0; i<args.size(); i++){
            signature.chordCallback.args[i] = args.get(i);
        }

        try{
            server.sendTask(signature.chordCallback);
        }catch (Exception e){
            throw new Exception("sendTask chordCallback error");
        }
    }

    public void taskFailed(Signature signature) throws Exception{
        try {
            server.backend.setStateFailure(signature);
        }catch (Exception e){
            throw new Exception("Set state failure error");
        }
    }


    public void taskRetry(Signature signature) throws Exception{
        try {
            server.backend.setStateRetry(signature);
        }catch (Exception e){
            throw new Exception("Set state retry error");
        }

        signature.retryCount--;
        signature.retryTimeout = Retry.fibonacciNext(signature.retryTimeout);

        Thread.sleep(signature.retryTimeout*1000);

        server.sendTask(signature);

    }


}
